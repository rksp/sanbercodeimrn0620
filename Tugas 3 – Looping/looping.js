/**
 * @file Tugas 3, Pekan 1, Sanbercode React Native Bootcamp, Batch Juni 2020
 * @author Riki Syahputra <kosiriki@gmail.com>
 */

/* Soal 1 */
console.log('\nLooping While');
console.log('LOOPING PERTAMA');
let counter = 0;
while(counter < 20) {
    counter += 2;
    console.log(counter + ' - I love coding');
}
console.log('LOOPING KEDUA')
while(counter > 0) {
    console.log(counter + ' - I will become a mobile developer');
    counter -= 2;
}

/* Soal 2 */
console.log('\nLooping menggunakan for');
console.log('OUTPUT');

function getString(num) {
    if(num % 3 === 0 && num % 2 !== 0) return 'I Love Coding';
    else if(num % 2 === 0) return 'Berkualitas';
    else return 'Santai';
}

for(let i = 1; i <= 20; i++) {
    console.log(getString(i))
}

/* Soal 3 */
function persegiPanjang (baris, kolom) {
    console.log('\nMembuat Persegi Panjang #')
    for(let b = 0; b < baris; b++) {
        let pagar;
        for(let k = 0; k < kolom; k++) {
            pagar = pagar ? pagar + '#' : '#';
        }
        console.log(pagar);
    }
}
persegiPanjang(4,8)

/* soal 4 */
function tangga (baris) {
    console.log('\nMembuat Tangga');
    for(let b = 0; b < baris; b++) {
        let pagar;
        for(let k = 0; k <= b; k++) {
            pagar = pagar ? pagar + '#' : '#';
        }
        console.log(pagar);
    }
}
tangga(6)

/* soal 5 */
function catur(baris, kolom) {
    console.log('\nMembuat Papan Catur')
    function getBaris(num) {
        return num % 2 === 0 ? ' ' : '#';
    }

    for(let i = 0; i <baris; i++) {
        let baris
        for(let j = 0; j < kolom; j++) {
            baris = baris ? baris + getBaris(i+j) : getBaris(i+j);
        }
        console.log(baris)
    }
}
catur(8,8);