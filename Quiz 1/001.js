/*
  A. Balik String (10 poin)
    Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst.

    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 

  B. Palindrome (10 poin)
    Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
    Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
    Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
    Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 

  
    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 
  
  C. Bandingkan Angka (10 poin)
    Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 
    
*/

function balikString(word) {
    var reversed = '';
    for(let i = word.length - 1; i >= 0; i--) {
        reversed = reversed + word.charAt(i);
    }
    return reversed
}

// (function() {
//     console.log('Testing function balikString');
//     const tables = [
//         { value: "abcde", expected: "edcba" },
//         { value: "rusak", expected: "kasur" },
//         { value: "racecar", expected: "racecar" },
//         { value: "haji", expected: "ijah" }
//     ];
//     tables.forEach(table => {
//         if(balikString(table.value) !== table.expected) throw new Error(`Soal pertama jawabannya salah bung!, harusnya ${table.expected} tapi munculnya ${balikString(table.value)}`);
//     })
// }())
  
function palindrome(word) {
    const length = word.length - 1;
    for(let i = 0; i <= length; i++) {
        if(word.charAt(i) !== word.charAt(length - i)) return false;
    }
    return true;
}

// (function(){
//     console.log('Testing function palindrome');
//     const tables = [
//         { value: "kasur rusak", expected: true },
//         { value: "haji ijah", expected: true },
//         { value: "nabasan", expected: false },
//         { value: "nababan", expected: true },
//         { value: "jakarta", expected: false },
//     ]
//     tables.forEach(table => {
//         if(palindrome(table.value) !== table.expected) throw new Error(`Soal kedua jawabannya salah om!, harusnya ${table.expected} tapi malah ${palindrome(table.value)}`);
//     })
// }())
  
function bandingkan(num1, num2) {
    if(num1 === num2 || num1 * num2 < 0) return -1;
    num1 = num1 || 0;
    num2 = num2 || 0;
    return +num1 > +num2 ? +num1 : +num2;
}

// (function(){
//     console.log('Testing function bandingkan');
//     const tables = [
//         { value: [10,15], expected: 15 },
//         { value: [12,12], expected: -1 },
//         { value: [-1,10], expected: -1 },
//         { value: [112,121], expected: 121 },
//         { value: [1], expected: 1 },
//         { value: [], expected: -1 },
//         { value: ["15","18"], expected: 18 },
//     ]

//     tables.forEach(table => {
//         const { value: [val1, val2], expected} = table;
//         const result = bandingkan(val1, val2);
//         if( result !== expected) throw new Error(`Soal ketiga jawabannya salah kak!, untuk soal ${val1} dan ${val2} harusnya ${expected} tapi outpunya malah ${bandingkan(val1, val2)}`);
//     })
// }())
  
  // TEST CASES BalikString

  console.log(balikString("abcde")) // edcba
  console.log(balikString("rusak")) // kasur
  console.log(balikString("racecar")) // racecar
  console.log(balikString("haji")) // ijah
  
  // TEST CASES Palindrome
  console.log(palindrome("kasur rusak")) // true
  console.log(palindrome("haji ijah")) // true
  console.log(palindrome("nabasan")) // false
  console.log(palindrome("nababan")) // true
  console.log(palindrome("jakarta")) // false
  
  // TEST CASES Bandingkan Angka
  console.log(bandingkan(10, 15)); // 15
  console.log(bandingkan(12, 12)); // -1
  console.log(bandingkan(-1, 10)); // -1 
  console.log(bandingkan(112, 121));// 121
  console.log(bandingkan(1)); // 1
  console.log(bandingkan()); // -1
  console.log(bandingkan("15", "18")) // 18