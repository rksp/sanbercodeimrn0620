/**
 * @file Tugas 5, Pekan 1, Sanbercode React Native Bootcamp, Batch June 2020
 * @author Riki Syahputra <kosiriki@gmail.com>
 */

/* utilities */
const isExist       = (...thing) => thing.indexOf(undefined) === -1 && thing.indexOf(null) === -1;
const isIdentic     = (thing1, thing2) => JSON.stringify(thing1) === JSON.stringify(thing2);
const sortFn        = (a,b) => +a === +b ? 0 : +a > +b ? 1 : -1;
const sortDescFn    = (a,b) => +a === +b ? 0: +a < +b ? 1 : -1;
const orderFn       = (n1, n2) => [n1, n2].sort(sortFn);

/* Soal 1 */
console.log('Soal No. 1 (Range)');
function range(num1, num2) {
    if(!isExist(num1, num2)) return -1;
    const [first, last] = orderFn(num1, num2);
    return Array.from({length: (last - first) + 1}, (_, index) => index + first);
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

/* Soal 2 */
console.log('\nSoal No. 2 (Range with Step)')
function rangeWithStep(num1, num2, step) {
    if(!isExist(num1, num2)) return -1;
    let arr = [];
    const [first, last] = orderFn(num1, num2);
    for(let i = first; i <= last; i += step) {
        arr.push(i);
    }
    return num2 >= num1 ? arr : arr.reverse();
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 5, 4)) // [29, 25, 21, 17, 13, 9, 5]

/* Soal 3 */
console.log('Soal No. 3 (Sum of Range)');
function sum(num1, num2, step = 1) {
    const arr = rangeWithStep(num1 || 0, num2 || 0, step);
    return arr.reduce((a,b) => a + b, 0);
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1 
console.log(sum()) // 0 

/* Soal 4 */
console.log('Soal No. 4 (Array Multidimensi)')
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(data) {
    return data.reduce((a,b) => {
        const [id, name, home, tl, hobby] = b;
        a.push({ 'Nomor ID': id, 'Nama Lengkap': name, 'TTL': home + ' ' + tl,  'Hobi':hobby });
        return a;
    },[]);
}

console.log(...dataHandling(input));

/* Soal 5 */
console.log('Soal No. 5 (Balik Kata)');
function balikKata(string) {
    let reversed = '';
    for(let i = string.length - 1; i >= 0; i--) {
        reversed = reversed + string.charAt(i);
    }
    return reversed;
}

console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar"));// racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 

/* Soal 6 */
console.log('\nSoal No. 6 (Metode Array)')
function dataHandling2(data) {
    console.log(data);
    const tanggal = data[3].split("/");
    switch(parseInt(tanggal[1])) {
        case 1: 
            console.log('Januari');
            break;
        case 2: 
            console.log('Februari');
            break;
        case 3: 
            console.log('Maret');
            break;
        case 4: 
            console.log('April');
            break;
        case 5: 
            console.log('Mei');
            break;
        case 6: 
            console.log('Juni');
            break;
        case 7: 
            console.log('Juli');
            break;
        case 8: 
            console.log('Agustus');
            break;
        case 9: 
            console.log('September');
            break;
        case 10: 
            console.log('Oktober');
            break;
        case 11: 
            console.log('November');
            break;
        case 12: 
            console.log('Desember');
            break;
    }
    console.log([...tanggal].sort(sortDescFn));
    console.log(tanggal.join("-"));
    console.log(new String(data[1]).slice(0,15));
}

dataHandling2(["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"])