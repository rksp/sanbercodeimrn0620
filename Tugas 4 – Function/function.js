/**
 * @file Tugas 4, Pekan 1, Sanbercode React Native Bootcamp, Batch June 2020
 * @author Riki Syahputra <kosiriki@gmail.com>
 */

 /* Soal 1 */
 function teriak() {
     return 'Halo Sanbers!';
 }

 console.log(teriak());

 /* Soal 2 */
 function kalikan(num1, num2) {
    return num1 * num2;
 }

 const num1 = 12;
 const num2 = 4;
 const hasil = kalikan(num1, num2);
 console.log(hasil);

 /* Soal 3 */
 function introduce(name, age, address, hobby) {
   return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`;
 }

 const name = "Agus";
 const age = 30;
 const address = "Jln. Malioboro, Yogyakarta";
 const hobby = "Gaming";
 
 const perkenalan = introduce(name, age, address, hobby);
 console.log(perkenalan);