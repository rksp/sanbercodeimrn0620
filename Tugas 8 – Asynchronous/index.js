var readBooks = require('./callback.js')
var readBookPromise = require('./promise');
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function readUsingCallback(time, num = 0) {
    if(!books[num] || time <= 0) {
        console.log('\nMembaca buku menggunakan Promise')
        readUsingPromise(10000)
        return
    }
    
    readBooks(time, books[num], function(time) {
        readUsingCallback(time, num + 1)
    })
}

function readUsingPromise(time, num = 0) {
    if(!books[num] || time <= 0) return
    readBookPromise(time, books[num])
        .then(function(time) { readUsingPromise(time, num + 1)})
        .catch(function(error) { console.log(error)})
}

console.log('Membaca buku menggunakan Callback');
readUsingCallback(10000)


