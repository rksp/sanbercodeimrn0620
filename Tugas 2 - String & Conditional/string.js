/**
 * @file Tugas 2 (string) Pekan 1, Sanbercode React Native Bootcamp, Batch Juni 2020
 * @author Riki Syahputra <kosiriki@gmail.com>
 */

/* Soal 1 */
console.log('\n' + 'Soal No. 1 (Membuat kalimat)');
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

const kalimat = word
    .concat(' ', second)
    .concat(' ', third)
    .concat(' ', fourth)
    .concat(' ', fifth)
    .concat(' ', sixth)
    .concat(' ', seventh);

// test sederhana 
// if(kalimat !== 'JavaScript is awesome and I love it!') throw new Error('Oops! Kalimatnya gak bener bro');

console.log(kalimat);

/* Soal 2 */
console.log('\n' + 'Soal No.2 Mengurai kalimat (Akses karakter dalam string)');
var sentence = "I am going to be React Native Developer"; 
const firstWord = sentence[0], 
    secondWord = sentence[2] + sentence[3], 
    thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9], 
    fourthWord = sentence[11] + sentence[12], 
    fifthWord = sentence[14] + sentence[15], 
    sixthWord = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21], 
    seventhWord = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28],
    eighthWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

// test sederhana
// if (firstWord !== 'I') throw new Error(`Oops! Soal kedua gak dijawab dengan benar bro!, kalimat 1 harusnya 'I', bukan ${firstWord}`);
// if (secondWord !== 'am') throw new Error(`Oops! Soal kedua gak dijawab dengan benar bro!, kalimat 2 harusnya 'am', bukan ${secondWord}`);
// if (thirdWord !== 'going') throw new Error(`Oops! Soal kedua gak dijawab dengan benar bro!, kalimat 3 harusnya 'going', bukan ${thirdWord}`);
// if (fourthWord !== 'to') throw new Error(`Oops! Soal kedua gak dijawab dengan benar bro!, kalimat 4 harusnya 'to', bukan ${fourthWord}`);
// if (fifthWord !== 'be') throw new Error(`Oops! Soal kedua gak dijawab dengan benar bro!, kalimat 5 harusnya 'be', bukan ${fifthWord}`);
// if (sixthWord !== 'React') throw new Error(`Oops! Soal kedua gak dijawab dengan benar bro!, kalimat 6 harusnya 'React', bukan ${sixthWord}`);
// if (seventhWord !== 'Native') throw new Error(`Oops! Soal kedua gak dijawab dengan benar bro!, kalimat 7 harusnya 'Native', bukan ${seventhWord}`);
// if (eighthWord !== 'Developer') throw new Error(`Oops! Soal kedua gak dijawab dengan benar bro!, kalimat 8 harusnya 'Developer', bukan ${eighthWord}`);

console.log('First Word: ' + firstWord); 
console.log('Second Word: ' + secondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord);
console.log('Eighth Word: ' + eighthWord);

/* Soal 3 */
console.log('\n' + 'Soal No. 3 Mengurai Kalimat (Substring)');
var sentence2 = 'wow JavaScript is so cool'; 
var firstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); 
var thirdWord2 = sentence2.substring(15, 17); 
var fourthWord2 = sentence2.substring(18, 20); 
var fifthWord2 = sentence2.substring(21); 

// test sederhana
// if(firstWord2 !== 'wow') throw new Error(`Oops! Soal ketiga jawabannya gak bener bro!, kalimat 1 harusnya 'wow', bukan ${firstWord2}`);
// if(secondWord2 !== 'JavaScript') throw new Error(`Oops! Soal ketiga jawabannya gak bener bro!, kalimat 2 harusnya 'JavaScript', bukan ${secondWord2}`);
// if(thirdWord2 !== 'is') throw new Error(`Oops! Soal ketiga jawabannya gak bener bro!, kalimat 3 harusnya 'is', bukan ${thirdWord2}`);
// if(fourthWord2 !== 'so') throw new Error(`Oops! Soal ketiga jawabannya gak bener bro!, kalimat 4 harusnya 'so', bukan ${fourthWord2}`);
// if(fifthWord2 !== 'cool') throw new Error(`Oops! Soal ketiga jawabannya gak bener bro!, kalimat 5 harusnya 'cool', bukan ${fifthWord2}`);

console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

/* Soal 4 */
console.log('\n' + 'Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String');
var sentence3 = 'wow JavaScript is so cool'; 
var firstWord3 = sentence3.substr(0, 3); 
var secondWord3 = sentence3.substr(4, 10);
var thirdWord3 = sentence3.substr(15, 2);
var fourthWord3 = sentence3.substr(18, 2);
var fifthWord3 = sentence3.substr(21);

var firstWordLength = firstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

// test sederhana
// if(firstWord3 !== 'wow') throw new Error(`Oops! Soal keempat jawabannya gak bener bro!, kalimat 1 harusnya 'wow', bukan ${firstWord3}`);
// if(secondWord3 !== 'JavaScript') throw new Error(`Oops! Soal keempat jawabannya gak bener bro!, kalimat 2 harusnya 'JavaScript', bukan ${secondWord3}`);
// if(thirdWord3 !== 'is') throw new Error(`Oops! Soal keempat jawabannya gak bener bro!, kalimat 3 harusnya 'is', bukan ${thirdWord3}`);
// if(fourthWord3 !== 'so') throw new Error(`Oops! Soal keempat jawabannya gak bener bro!, kalimat 4 harusnya 'so', bukan ${fourthWord3}`);
// if(fifthWord3 !== 'cool') throw new Error(`Oops! Soal keempat jawabannya gak bener bro!, kalimat 5 harusnya 'cool', bukan ${fifthWord3}`);

console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 