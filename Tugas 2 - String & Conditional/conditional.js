/**
 * @file Tugas 2 (conditional) Pekan 1, , Sanbercode React Native Bootcamp, Batch Juni 2020
 * @author Riki Syahputra <kosiriki@gmail.com>
 */

 /* soal 1 */
 function game(nama, peran) {
     if(!nama) {
        return console.log('Nama harus diisi!');
     }

     if(!peran) {
        return console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
     }

     console.log(`Selamat datang di Dunia Werewolf, ${nama}`);

     if(peran === 'Penyihir') {
        return console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
     }

     else if(peran === 'Guard') {
        return console.log(`Halo ${peran} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`);
     }

     else if(peran === 'Werewolf') {
        return console.log(`Halo ${peran} ${nama}, Kamu akan memakan mangsa setiap malam!`);
     }
 }

//  test sederhana
//  (function() {
//     const _log = console.log;
//     console.log = string => string;
//     if(game('', '') !== "Nama harus diisi!") throw new Error('Oops! Soal 1, urutan 1 jawabannya salah bro!');
//     if(game('John', '') !== "Halo John, Pilih peranmu untuk memulai game!") throw new Error('Oops! Soal 1, urutan 2 jawabannya salah bro!');
//     if(game('Jane', 'Penyihir') !== "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!") throw new Error('Oops! Soal 1, urutan 3 jawabannya salah bro!');
//     if(game('Jenita', 'Guard') !== "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.") throw new Error('Oops! Soal 1, urutan 4 jawabannya salah bro!');
//     if(game('Junaedi', 'Werewolf') !== "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" ) throw new Error('Oops! Soal 1, urutan 5 jawabannya salah bro!');
//     console.log = _log;
//  }());

 game('', '');
 game('John', '');
 game('Jane', 'Penyihir');
 game('Jenita', 'Guard');
 game('Junaedi', 'Werewolf');

 /* soal 2 */
function tanggal(hari, bulan, tahun) {
    let _bulan;
    switch(bulan) {
        case 1: 
            _bulan = 'Januari';
            break;
        case 2: 
            _bulan = 'Februari';
            break;
        case 3: 
            _bulan = 'Maret';
            break;
        case 4: 
            _bulan = 'April';
            break;
        case 5: 
            _bulan = 'Mei';
            break;
        case 6: 
            _bulan = 'Juni';
            break;
        case 7: 
            _bulan = 'Juli';
            break;
        case 8: 
            _bulan = 'Agustus';
            break;
        case 9: 
            _bulan = 'September';
            break;
        case 10: 
            _bulan = 'Oktober';
            break;
        case 11: 
            _bulan = 'November';
            break;
        case 12: 
            _bulan = 'Desember';
            break;
    }

    return hari + ' ' + _bulan + ' ' + tahun;
} 


// test sederhana
// if(tanggal(21, 1, 1945) !== '21 Januari 1945') throw new Error('Soal ke 2 jawabannya salah bro!');

const hari = 21; 
const bulan = 1; 
const tahun = 1945;

const hasil = tanggal(hari, bulan, tahun);
console.log(hasil)